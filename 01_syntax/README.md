---

# GO  Syntax

---

# GO Syntax

a GO file consist of following parts:

- Package declaration
- Import Packages
- Functions
- Statements and expressions

Lets take a look on code to understand it better:

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello World !")
}
```

---

# GO syntax

- Line 1: In Go, every program is part of a package. We define this using the package keyword. In this example, the program belongs to the `main` package.
- Line 2: `import ("fmt")` lets us import files included in the fmt package.
- Line 3: A blank line. Go ignores white space. Having white spaces in code makes it more readable.
- Line 4: `func main() {}` is a function. Any code inside its curly brackets `{}` will be executed.
- Line 5: `fmt.Println()` is a function made available from the `fmt` package. It is used to output/print text. In our example it will output "Hello World!". 

---

# GO statements 

In GO language, statements are separated by ending the line, or by semicolon `;`
The left curly bracket { cannot come at the start of a line.
For example, code below will return `syntax error`

```go
package main
import ("fmt")

func main()
{
  fmt.Println("Hello World!")
} 
```

---

# GO compact code

You can write more compact manner as a one liner, like shown below, But it is highly not recommended due to difficult to read or debug the code later on:
```go
 package main; import ("fmt"); func main() { fmt.Println("Hello World!");}
```

---

# GO single-line comments

- Single-line comments start with two slashes `//`
- Any text after `//` to the end of line is ignored.

```go
// This is a comment
package main
import ("fmt")

func main() {
  // This is a comment
  fmt.Println("Hello World!")
  fmt.Println("World Hello!") // This is a comment
}
```

---

# GO multi-line comments


Multi-line comments start with /* and ends with */.

Any text between /* and */ will be ignored by the compiler:

```go
package main
import ("fmt")

func main() {
  /* The code below will print Hello World
  to the screen, and it is amazing */
  fmt.Println("Hello World!")
}
```

---

# Practice

- Create example.com module
- Copy code from single-line comments to `hello.go` file
- Comment out the line with `hello world`` print
- Run the code
- Compile and run


> `[!]` Note: You can also use comments to prevent the code from being executed. 


