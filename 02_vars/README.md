---

 # GO Variables

---

# GO variables

Variables are containers for storing data values

### GO variables types

In GO there are different types of variables:

- `int`: stores integers
- `float32`: stores floating point numbers, with decimals
- `string`: stores test such as "hello world", String values are surrounded by double quotes
- `bool`: stores variables with `true` or `false`  value

>  Note: it is suggested to read about [GO Data Types](https://www.w3schools.com/go/go_data_types.php) in official documentation

---

# Declaring variables

In GO, there are two ways to declare a variables:

- With `var` keyword:
    - `var my_var_name type = value`
    - `var my_hobby string = "Warhammer 40k"`
    - `Note`: You always have to specify either type or value (or both).
- With Walrus operator --> `:=`
    - Use walrus operator followed by the variable value
        - `myName := "Silent-Mobius"`
        - `Note`: In this case, the type of the variable is inferred from the value (means that the compiler decides the type of the variable, based on the value).
        - `Note`: It is **not** possible to declare a variable using :=, without assigning a value to it.

---

# Variable declaration _with_ initial value

If the value of a variable is known from the start, you can declare the variable and assign a value to it on one line:

```go 
package main
import ("fmt")

func main() {
  var student1 string = "John" //type is string
  var student2 = "Jane" //type is inferred
  x := 2 //type is inferred

  fmt.Println(student1)
  fmt.Println(student2)
  fmt.Println(x)
}
```
> Note: The variable types of student2 and x is inferred from their values

---

# Variable declaration _Without_ initial Value

In Go, all variables are initialized. So, if you declare a variable without an initial value, its value will be set to the default value of its type: 

```go
package main
import ("fmt")

func main() {
  var a string
  var b int
  var c bool

  fmt.Println(a)
  fmt.Println(b)
  fmt.Println(c)
}
```

### Example explained

In this example there are 3 variables:

- a
- b
- c

These variables are declared but they have not been assigned initial values.

By running the code, we can see that they already have the default values of their respective types:

- a is ""
- b is 0
- c is false

---

# Value assignment after declaration

It is possible to assign a value to a variable after it is declared. This is helpful for cases the value is not initially known.

```go
package main
import ("fmt")

func main() {
  var student1 string
  student1 = "John"
  fmt.Println(student1)
}
```
> `Note` : It is not possible to declare a variable using ":=" without assigning a value to it.

---

# Difference between `var` and `:=`

|  var |  := |
| ---  | --- |
|Can be used inside and outside of functions | Can only be used inside functions |
| Variable declaration and value assignment can be done separately | Variable declaration and value assignment cannot be done separately (must be done in the same line) |

```go
package main
import ("fmt")

var a int
var b int = 2
var c = 3

func main() {
  a = 1
  fmt.Println(a)
  fmt.Println(b)
  fmt.Println(c)
}
```

The example above shows declaration variables outside of a function, with the `var` keyword.

---

```go
package main
import ("fmt")

a := 1

func main() {
  fmt.Println(a)
}
```
