---

pagination: true

---

# Go Programming basics

---

# What is GO ?

- Go is cross-platform, open source programming language
- Go can be used to create high-performance applications
- Go is fast, statically typed, compiled language, with dynamically type feel to it.
- Go's syntax is similar to C++


---

# What is GO used for ?

- Web server side development
- Network-based programs
- Developing cross-platform enterprise applications
- Cloud-native development

---

# Why use GO?
- Go is Gun and easy to learn
- Go has fast run time and compilation time
- Go supports concurrency
- Go has memory management
- Go works on multiple platforms

---

# Getting started

This tutorial will teach you the basics of Go.
It is not necessary to have any prior programming experience.

---

# Setup 

To start using Go, you need two things:

- A text editor, like vim,VSCode, goland
    - Any text editor you feel comfortable should be sufficient, but above mentioned, have plugins that easy on your development experience
- A compiler, like GCC, to translate the Go code into a language that the computer will understand

---

# Setup (cont.)

### Installing GO compiler

we can download the compiler from [Golang.org](https://golang.org/dl/).

Follow the instructions related to your operating system, iin my case Linux based system. To check if `GO` was installed successfully, you can run the following command in a terminal window:

```sh
go version
```
It should return version of installed version.

---

# Setup (cont.)

### Installing editor

I am using vscode clone named [vscodium](https://vscodium.com), with `GO` plugin for language integration, yet as mentioned, you are welcome to use what ever you wish.


---

# Setup (cont.)

### Configure editor

- Install GO plugins
- Install and configure version control


---

# Setup (cont.)

### Hello world

```sh
 go mod init example.com/hello
```

```go
package main
import ("fmt")

func main() {
  fmt.Println("Hello World!")
} 
```

```sh
 go run ./helloworld.go 
```


---

# Practice

- Setup GO compiler
- Install editor and its plugins
- Create GO `example.com` project
- Copy `Hello World` code
- Run the code
- Compile the code and run it.


